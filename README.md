# Sentiment Analysis on Structured Data

Sentiment analysis involves determining the polarity of texts and documents. It can be used for binary classification - positive or negative sentiment or for the more challenging task of fine-grained sentiment classification. 

Structured data could be sequences, hierarchical  structures(e.g trees), graphs, relational databases and so on. In the case of the Stanford Sentiment Treebank(SST), the corpus has fully labelled parse trees. Given the structure of these datasets, most machine learning methods are unable to capture in a natural way the complexity of a structured data since they are designed for data represented in tabular form. 

There however are models designed to cater for these kind of datasets such as Recursive Neural Networks. Recursive Neural Networks comprise a class of architecture that can operate on structured input. 

Fine-grained sentiment analysis typically uses five discrete classes and in the case of SST, the classes are: 1 - Very Negative, 2 - Negative, 3 - Neutral, 4 - Positive, 5 - Very Positive. The paper released originally with the data(using Recursive Neural Tensor Network) achieved at the time an accuracy of 45.7\%.

### Dataset 

The Stanford Sentiment Treebank consists of 11,855 single sentences extracted from movie reviews. It was parsed with Stanford parser and includes a total of 215,154 unique phrases. 

### Methods and Experiments

The methods employed in this project are: Logistic Regression, Recurrent Neural Network and Recursive Neural Network.

##### Logistic Regression

Logistic Regression can be used to classify observations into one of two classes or into one of many classes in our case. It is one of the most commonly used supervised learning algorithms for classification.

Since Logistic Regression is a feature-based method, the words/texts are are transformed into features. First of all, the tree structure of phrases is converted into raw text and its associated label in a tabular form. Given the structure of the Stanford Sentiment Treebank, I prepared two datasets - a dataset of full sentences and their labels and another dataset with all unique phrases.

To transform the text into features, I used scikit's CountVectorizer which converts the training data into a matrix of token counts. It removes the stop words by default and produces a sparse representation of the counts. 

The count matrix is then converted to a TF-IDF(Term-frequency Inverse Document Frequency) representation. The goal of using tf-idf instead of raw frequencies of occurrence of a token in a given document is to scale down the impact of tokens that occur very frequently in a given corpus and that are hence empirically less informative.

After obtaining the tf-idf representation, the Logistic Regression is trained by fitting it to the features and class labels are predicted for the test samples.

The results for the two dataset are presented below, it can be observed that greater test accuracy is achieved on the 'all-phrases' dataset.

|                 | FUll Sentence | All-Phrases |
|-----------------|---------------|-------------|
|LR test accuracy | 40.18%        | 76.03%      |


##### Recurrent Neural Network

Recurrent Neural Network extends neural networks to process input of variable lengths by keeping an hidden state, hence they are commonly used in analysing sequences. It takes in sequence of words one at a time and produces a hidden state for each word. After getting the  final hidden state, it is then fed through a linear layer to receive the predicted sentiment.

The datasets require a different preparation since I used the PyTorch deep learning framework. From TorchText, the 'Field' class is used to load the datasets and 'BucketIterators' is used to create batches for training iterations using a batch size of 32. I cleaned the raw texts by removing punctuations and new lines and the texts were tokenized using the spacy tokenizer.

For training, instead of having the word embeddings initialized randomly, I used pretrained  embeddings - the glove.840B.300d, whicht was trained on 840 billion tokens and the embedding layer/vectors are size 300.

To prevent overfitting, I applied the dropout technique on the embedding layer and the final hidden layer. Adam optimizer was used in training.

Similar to the logistic regression, I trained the RNN on two datasets - the full sentence datasets and all-phrases datasets with the following results:

|                 | FUll Sentence | All-Phrases |
|-----------------|---------------|-------------|
|RNN val accuracy | 37.27%        | 80.68%      |
|RNN test accuracy | 36.25%        | 80.18%      |


##### Recursive Neural Network

Recursive neural networks comprise a class of architecture that can operate on structured input. 

Recursive  neural  networks comprise  an  architecture  in  which the same set of weights is recursively applied within a structural setting:  given a positional directed acyclic graph,it visits the nodes in topological order, and recursively applies transformations to generate further representations from previously computed representations of children.

For this model, I kept the structure of the corpus as originally provided by the authors since this class of algorithms can work with tree structures. There was however the need to create special functions to handle the tree traversal and node loss computation.

The model performed better on the test set when trained using dropout technique at the embedding layer. Adam optimizer improved the performance of the model over the standard stochastic gradient descent.

The architecture is a very difficult one to train. Limited by computing resources, I had to create checkpoints to save the model states  and resuming training from the checkpoint.

|                 | FUll Sentence | All-Phrases |
|-----------------|---------------|-------------|
|Recursive NN val accuracy | 31.27%        | 76.01%      |
|Recursive NN test accuracy | 32.61%        | 73.28%      |


### Conclusion

|                 | FUll Sentence | All-Phrases |
|-----------------|---------------|-------------|
|LR test accuracy | 40.18%        | 76.03%      |
|RNN test accuracy | 36.25%        | 80.18%      |
|Recursive NN test accuracy | 32.61%        | 73.28%      |

With more computing resources and training time, it seems the recursive neural network might have performed better, but this is one disadvantage of the class of algorithms. It takes a long time to achieve good performance.

The Logistic Regression performed best on the full sentence classification while the recurrent neural network performed better on all-phrases sentiment analysis.

One area of improvement could be replacing the standard recurrent neural network with the newer Long Short-Term Memory(LSTM) architecture which is a variant of RNN and mitigates the vanishing gradient problem inherent in the standard recurrent neural network by having an extra recurrent state.
